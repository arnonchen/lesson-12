#pragma once
#include <iostream>
#include <string>
#include <vector>
class MessagesSender
{

public:
	void signin(std::string name);
	void signout(std::string name);
	void ConnectedUsers();
	int getConnectedUsers();
	std::string getUser(int index);
	MessagesSender();
	std::vector<std::string> _connected_users;
};
