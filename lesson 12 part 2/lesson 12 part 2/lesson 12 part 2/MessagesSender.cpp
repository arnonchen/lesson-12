#include "MessagesSender.h"
#include <iostream>
#include <algorithm>
MessagesSender::MessagesSender()
{

}

void MessagesSender::signin(std::string name)
{
	
	if (std::find(_connected_users.begin(), _connected_users.end(), name) != _connected_users.end())
	{
		std::cout << "user already connected\n";
		return;
	}
	
	std::cout << "signing in...\n";
	_connected_users.push_back(name);
}

void MessagesSender::signout(std::string name)
{
	if (std::find(_connected_users.begin(), _connected_users.end(), name) != _connected_users.end())
	{
		_connected_users.erase(std::remove(_connected_users.begin(), _connected_users.end(), name), _connected_users.end());
		std::cout << "signing out...\n";
		return;
	}
	std::cout << "user not found\n";
}


void MessagesSender::ConnectedUsers()
{
	std::cout << "connected users are:\n";
	for (int i = 0; i < _connected_users.size(); i++)
	{
		std::cout << _connected_users[i] << "\n";
	}
}

int MessagesSender::getConnectedUsers()
{
	return _connected_users.size();
}

std::string MessagesSender::getUser(int index)
{
	return _connected_users[index];
}