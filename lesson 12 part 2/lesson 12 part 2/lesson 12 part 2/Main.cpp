#include "MessagesSender.h"
#include <iostream>
#include <fstream>
#include <queue>
#include <string>
#include <thread>
#include <Windows.h>
#include <mutex>


void writeToFile();
void userInterface();
void readFromFile();

int option = 0;
using namespace std;
MessagesSender sender;
std::queue<std::string> messages;
std::mutex c;


int main()
{
	std::thread t1(writeToFile);
	std::thread t2(readFromFile);
	userInterface();
	t1.detach();
	t2.detach();
	return 0;

}

void readFromFile()
{
	std::string line;
	while (true)
	{
		std::unique_lock<std::mutex> locker(c);
		ifstream file("data.txt");
		while (std::getline(file, line))
		{
			if (line.size() > 0)
			{
				messages.push(line);
			}
		}
		locker.unlock();
		std::ofstream fileInOf("data.txt");
		fileInOf.open("data.txt", ios::out | ios::trunc);
		fileInOf.close();
		file.close();
		Sleep(60000);
	}
}

void writeToFile()
{
	std::string line;
	std::vector<std::string>::iterator it;
	while (true)
	{
		std::unique_lock<std::mutex> locker(c);
		while (!(messages.empty()))
		{
			std::ofstream file("output.txt", std::ios::app);
			line = messages.front();
			messages.pop();
			for (it = sender._connected_users.begin(); it != sender._connected_users.end(); it++)
			{
				file << *it << ": " << line << "\n";
			}
			file.close();
		}
		locker.unlock();
	}
}

void userInterface()
{
	std::string username;
	while (option != 4)
	{
		std::cout << "welcome to MessageSender\nenter:\n1 - to sign in a disconected user\n2 - to sign out a connected user\n3 - to print all connected users\n4 - to exit\n";
		std::cin >> option;
		switch (option)
		{
		case 1:
			std::cout << "enter your username:\n";
			std::cin >> username;
			sender.signin(username);
			break;
		case 2:
			std::cout << "enter your username:\n";
			std::cin >> username;
			sender.signout(username);
			break;
		case 3:
			sender.ConnectedUsers();
			break;
		case 4:
			std::cout << "bye!";
			break;
		default:
			std::cout << "not an option";
			break;
		}
	}
}