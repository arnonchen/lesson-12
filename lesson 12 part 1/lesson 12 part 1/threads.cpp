#include "threads.h"
#include <iostream>
#include <mutex>
#include <thread>
std::mutex m;
void I_Love_Threads()
{
	std::cout << "I Love Threads\n";
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int flag = 0;
	for (int i = begin; i <= end; i++)
	{
		if (i != 1 && i != 0)
		{
			flag = 1;
		}
		for (int j = 2; j <= i / 2; ++j)
		{
			if (i % j == 0)
			{
				flag = 0;
				break;
			}
		}
		if (flag == 1)
		{
			primes.push_back(i);
		}
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	thread t1(getPrimes,begin,end,std::ref(primes));
	t1.join();
	std::chrono::steady_clock::time_point finish = std::chrono::steady_clock::now();
	std::cout << "time: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << " ms" << std::endl;
	return primes;
}

void printVector(vector<int> primes)
{
	for (int i = 0; i < primes.size(); ++i)
	{
		std::cout << primes[i] << "\n";
	}
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	int flag = 0;
	for (int i = begin; i <= end; i++)
	{
		if (i != 1 && i != 0)
		{
			flag = 1;
		}
		for (int j = 2; j <= i / 2; ++j)
		{
			if (i % j == 0)
			{
				flag = 0;
				break;
			}
		}
		if (flag == 1)
		{
			m.lock();
			file << i << "\n";
			m.unlock();
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	vector<thread> threads;
	ofstream file(filePath);
	int a = end - begin;
	a /= N;
	for (int i = begin; i < end; i += a)
	{
		if ((i + a) > end)
		{
			threads.emplace_back(thread(writePrimesToFile, i, end, ref(file)));
			break;
		}
		threads.emplace_back(thread(writePrimesToFile, i, (i + a), ref(file)));
	}
	for (int i = 0; i < threads.size(); i++)
	{
		threads[i].join();
	}
	std::chrono::steady_clock::time_point finish = std::chrono::steady_clock::now();
	std::cout << "time: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << " ms" << std::endl;
}