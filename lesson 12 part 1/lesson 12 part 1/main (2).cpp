#include "threads.h"
#include <mutex>

int main()
{
	call_I_Love_Threads();
	vector<int> primes1;
	getPrimes(58, 100, primes1);
	std::mutex m;
	vector<int> primes3 = callGetPrimes(93, 289);
	vector<int> primes4 = callGetPrimes(0, 1000);
	vector<int> primes5 = callGetPrimes(0, 100000);
	vector<int> primes6 = callGetPrimes(0, 1000000);//it takes a long time so youve been warned
	callWritePrimesMultipleThreads(1, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(1, 1000000, "primes2.txt", 2);//this takes a long time too
	system("pause");
	return 0;
}